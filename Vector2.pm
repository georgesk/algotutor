# Author:  Chao-Kuei Hung
# For more info, including license, please see doc/index.html

package Vector2;
# 2-d Vector

use strict;
use Carp;
use vars qw(@ISA);
@ISA = qw(Vector);

use Vector;

#sub slope {
#    my ($self) = @_;
#    return $self->[1]/$self->[0];
#}

sub signed_area {
    my ($a, $b) = @_;
    return $a->x*$b->y - $a->y*$b->x;
}

sub signed_turn {
    my ($a, $b) = @_;
    return atan2($a->signed_area($b), $a->dot($b));
}

#sub makebasis {
#    my ($self) = @_;
#    my ($l) = $self->norm();
#    my ($c, $s) = ($self->[0]/$l, $self->[1]/$l);
#    return [Vector->new($c,-$s),Vector->new($s,$c)];
#}

if ($0 =~ /Vector2.pm$/) {
# being tested as a stand-alone program, so run test code.
    my ($p, $q);
    $p = Vector2->new(4,-3);
    $q = Vector2->new(5,12);
    print $p+$q, ",", $p-$q, "\n";
    # ans: -0.6435
    print $p->signed_area($q), "\n";
    # ans: 63
    $p = Vector2->new(-sqrt(3),1);
    $q = Vector2->new(-1,-1);
    print $p->signed_turn($q), "\n";
    # ans: 1.309
}

1;

