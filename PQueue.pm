# Author:  Chao-Kuei Hung
# For more info, including license, please see doc/index.html

package PQueue;
# Abstract Priority Queue

use strict;
use Carp;
use vars qw(@ISA);
@ISA = qw(Collection);

use Collection;
use overload
    '""' => 'stringify',
    'fallback' => undef
;

sub new {
    my ($class, %opts) = @_;
    $class = ref($class) if ref($class);
    my ($self) = $class->SUPER::new(%opts);
    $self->{"#data"} = [0];
#    if (ref($class)) {
#	$Data::Dumper::Terse = 1;
#	return eval(Dumper($class));	# cloning
#    }
    return $self;
}

sub size {
    my ($self, $newsize) = @_;
    $#{$self->{"#data"}} = $newsize if (defined $newsize);
    return $#{$self->{"#data"}};
}

sub is_empty {
    my ($self) = @_;
    return $self->size() <= 0;
}

sub stringify {
    my ($self) = @_;
    return "PQ[" . join(" ", @{$self->{'#data'}}[1..$self->size()]) . "]";
}

sub find {
    my ($self, $target) = @_;
    my ($i);
    for ($i=$self->size(); $i>0; --$i) {
	last if $self->{"#data"}[$i] eq $target;
    }
    return $i;
}

$::Config->{PQueue} = {
};

if ($0 =~ /PQueue.pm$/) {
# being tested as a stand-alone program, so run test code.

my ($q) = PQueue->new();
print "<$q>\n";

}

1;

