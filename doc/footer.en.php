    <hr />

    <ul>
      <?php include "$top[fs]/i/footer.en.php" ?>

      <li>You are welcome to distribute this document in
      accordance with the <a rel="license" href=
      "http://creativecommons.org/licenses/by-sa/2.5/">
      Creative Commons Attribution-ShareAlike License</a>
      or the <a rel="license" href = "<?= $top["url"] ?>/i/fdl.html">
      Free Document License</a>.</li>
    </ul>

