<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- <base href = "http://localhost/~ckhung/"> -->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator"
  content="HTML Tidy for Linux/x86 (vers 1st March 2003), see www.w3.org" />
  <?php include "../../i/meta.php" ?>

  <title>My Personal Notes</title>
</head>

<body>
  
<?php include "header.en.php" ?>
<div id="content">

  <h1>My Personal Notes</h1>
  <hr />

  <p>Sorry, this file is pretty much for my own use only. It might one
  day become a document for the internal workings of algotutor, if I
  ever get serious about it. Right now I only list nasty bugs that each
  took me several days to figure out.</p>

  <h2>Nasty Bugs to Remember</h2>

  <ol>
    <li>Time Knob makes clock out of sync due to different resolution
    level among windows. RecDialog.pm::seek_bkwd_at_level and
    RecDialog.pm::seek_fwd_at_level.</li>

    <li>Superfluous "-name" while inserting a Vertex as -content into a
    Heap makes the program display incorrectly but still run correctly.
    graph/pfs</li>
  </ol>
  
<?php include "footer.php" ?>
</div>
<?php include "$top[fs]/i/navigator.php" ?>

</body>
</html>
