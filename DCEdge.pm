# Author:  Chao-Kuei Hung
# For more info, including license, please see doc/index.html

package DCEdge;
# Edge in a Doubly-Connected Edge List

use strict;
use Carp;
use vars qw(@ISA);
@ISA = qw(Edge);

use Edge;

# sub new { my ($self) = shift; $self->SUPER::new(@_); }

sub phantomize {
    my ($self) = @_;
    $self->{"#is_phantom"} = 1;
    $self->configure(-status=>"hidden");
}

sub is_phantom {
    return $_[0]->{"#is_phantom"};
}

sub twin {
    my ($self, $nv) = @_;
    my ($r) = $self->{adj}{twin};
    $self->{adj}{twin} = $nv if $#_ >= 1;
    return $r;
}

sub prev {
    my ($self, $nv) = @_;
    my ($r) = $self->{adj}{prev};
    $self->{adj}{prev} = $nv if $#_ >= 1;
    return $r;
}

sub next {
    my ($self, $nv) = @_;
    my ($r) = $self->{adj}{next};
    $self->{adj}{next} = $nv if $#_ >= 1;
    return $r;
}

sub configure {
    my ($self, %opts) = @_;
    $self->SUPER::configure(%opts);
    $self->twin()->SUPER::configure(%opts)
	if (not $self->cget(-directed) and ref $self->twin());
}

$::Config->{DCEdge} = {
};

1;

